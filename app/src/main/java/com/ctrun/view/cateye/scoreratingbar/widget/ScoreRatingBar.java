package com.ctrun.view.cateye.scoreratingbar.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.ctrun.view.cateye.scoreratingbar.R;
import com.ctrun.view.cateye.scoreratingbar.databinding.StarProgressViewBinding;

/**
 * @author ctrun on 2021/10/19.
 */
public class ScoreRatingBar extends LinearLayout {
    public ScoreRatingBar(Context context) {
        this(context, null);
    }

    public ScoreRatingBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScoreRatingBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private static final int STAR_COUNT = 5;

    private final Bitmap mStarBitmap;
    private final StarProgressViewBinding[] mStarProgressViewBindings = new StarProgressViewBinding[STAR_COUNT];
    {
        mStarBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_star_score_yellow);

        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        for (int i = 0; i < STAR_COUNT; i++) {
            mStarProgressViewBindings[i] = StarProgressViewBinding.inflate(layoutInflater, this, false);
            addView(mStarProgressViewBindings[i].getRoot());
        }
    }

    public void setRating(float rating) {
        //重置
        for (int i = 0; i < STAR_COUNT; i++) {
            mStarProgressViewBindings[i].ivProgress.setImageBitmap(null);
        }

        int position = (int) (rating / 2);
        for (int i = 0; i < position; i++) {
            mStarProgressViewBindings[i].ivProgress.setImageResource(R.drawable.ic_star_score_yellow);
        }

        if (rating % 2 != 0) {
            float progress = rating % 2 / 2;
            Bitmap bitmap = createStarProgressBitmap(progress);
            mStarProgressViewBindings[position].ivProgress.setImageBitmap(bitmap);
        }

    }

    public void setRatingAlpha(int alpha) {
        for (StarProgressViewBinding binding : mStarProgressViewBindings) {
            binding.ivProgress.setImageAlpha(alpha);
            binding.ivProgressBackground.setImageAlpha(alpha);
        }
    }

    private Bitmap createStarProgressBitmap(float progress) {
        if (progress == 0) {
            return null;
        }

        if (mStarBitmap == null) {
            return null;
        }

        final int width = mStarBitmap.getWidth();
        final int height = mStarBitmap.getHeight();
        final int currentWidth = (int) (width * progress);

        if (currentWidth == 0) {
            return null;
        }

        Matrix matrix = new Matrix();

        Bitmap resultBitmap = Bitmap.createBitmap(mStarBitmap, 0, 0, currentWidth, height, matrix, false);

        return resultBitmap;
    }

}
