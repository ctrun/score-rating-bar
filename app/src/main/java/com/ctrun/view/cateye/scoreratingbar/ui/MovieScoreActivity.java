package com.ctrun.view.cateye.scoreratingbar.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ctrun.view.cateye.scoreratingbar.databinding.ActivityMovieScoreBinding;

/**
 * @author ctrun on 2022/8/5.
 */
public class MovieScoreActivity extends AppCompatActivity {

    private ActivityMovieScoreBinding mBinding;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityMovieScoreBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());

        mBinding.flBack.setOnClickListener(v -> onBackPressed());
        mBinding.seekBar.setOnSeekBarChangeListener(mSeekBarChangeListener);

        mBinding.tvTitle.setText("明日战记");
        mBinding.seekBar.setProgress(91);
        String imgUrl = "https://p0.pipi.cn/mmdb/25bfd63302f0fa395b07accde068bfd3c361f.jpg?imageMogr2/thumbnail/447x447";
        Glide.with(this).load(imgUrl).apply(RequestOptions.centerCropTransform().dontAnimate()).into(mBinding.ivToolbarMovieImg);
    }

    SeekBar.OnSeekBarChangeListener mSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            float rating = progress / 10f;
            mBinding.scoreRatingBar.setRating(rating);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

}
